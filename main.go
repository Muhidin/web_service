package main

import (
	"fmt"
	"net/http"

	. "web_service/database"

	_ "github.com/go-sql-driver/mysql"
)

// type student struct {
// 	Id    string
// 	Name  string
// 	Age   int
// 	Grade int
// }

// func connect() (*sql.DB, error) {
// 	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
// 	if err != nil {
// 		return nil, err
// 	}

// 	return db, nil
// }

// func getUsers(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-Type", "application/json")

// 	if r.Method == "GET" {

// 		var limit = r.URL.Query()["per_page"]
// 		var offset = r.URL.Query()["page"]

// 		limit1 := limit[0]
// 		offset1 := offset[0]

// 		// db, err := connect()
// 		db, err := ConnectDB()
// 		if err != nil {
// 			fmt.Println(err.Error())
// 			return
// 		}
// 		defer db.Close()

// 		rows, err := db.Query("SELECT id,name,age,grade FROM tb_student LIMIT ? OFFSET ?", limit1, offset1)
// 		if err != nil {
// 			fmt.Println(err.Error())
// 			return
// 		}
// 		defer rows.Close()

// 		var res []student

// 		for rows.Next() {
// 			var s = student{}
// 			var err = rows.Scan(&s.Id, &s.Name, &s.Age, &s.Grade)

// 			if err != nil {
// 				fmt.Println(err.Error())
// 				return
// 			}

// 			res = append(res, s)
// 		}

// 		var result, err2 = json.Marshal(res)
// 		if err2 != nil {
// 			http.Error(w, err2.Error(), http.StatusInternalServerError)
// 			return
// 		}

// 		w.Write(result)
// 		return

// 	}

// 	http.Error(w, "", http.StatusBadRequest)
// }

func main() {
	http.HandleFunc("/users", GetUsers)
	http.HandleFunc("/addUser", PostUser)
	http.HandleFunc("/getById", GetUserById)
	http.HandleFunc("/updateUser", UpdateUser)
	http.HandleFunc("/deleteUser", DeleteUser)

	fmt.Println("Web server started at localhost:8000")
	http.ListenAndServe(":8000", nil)
}
