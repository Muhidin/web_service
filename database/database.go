package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

type student struct {
	Id    string
	Name  string
	Age   int
	Grade int
}

type response struct {
	Error   bool
	Message string
	Data    []student
}

func ConnectDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
	if err != nil {
		return nil, err
	}

	return db, nil
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if r.Method == "GET" {

		var limit = r.URL.Query()["per_page"]
		var offset = r.URL.Query()["page"]

		limit1 := limit[0]
		offset1 := offset[0]

		// db, err := connect()
		db, err := ConnectDB()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer db.Close()

		rows, err := db.Query("SELECT id,name,age,grade FROM tb_student LIMIT ? OFFSET ?", limit1, offset1)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()

		var res []student

		for rows.Next() {
			var s = student{}
			var err = rows.Scan(&s.Id, &s.Name, &s.Age, &s.Grade)

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			res = append(res, s)
		}

		if res == nil {
			var obj interface{} = &response{Error: false, Message: "Data tidak ada", Data: res}
			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, err2.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(result)
			return
		}

		var obj interface{} = &response{Error: false, Message: "Success get data", Data: res}

		var result, err2 = json.Marshal(obj)
		if err2 != nil {
			http.Error(w, err2.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(result)
		return

	}

	http.Error(w, `{"Error": true, "Message": "Hanya method GET yang dipakai"}`, http.StatusBadRequest)
}

func GetUserById(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")

	if r.Method == "GET" {
		var id = r.URL.Query()["id"]

		db, err := ConnectDB()
		if err != nil {
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		}

		defer db.Close()

		rows, err := db.Query("SELECT id,name,age,grade FROM tb_student WHERE id = ?", id[0])
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()

		var res []student

		for rows.Next() {
			var s = student{}
			var err = rows.Scan(&s.Id, &s.Name, &s.Age, &s.Grade)

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			res = append(res, s)
		}

		if res == nil {
			var obj interface{} = &response{Error: true, Message: "Data tidak ada", Data: res}
			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, err2.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(result)
			return
		}

		var obj interface{} = &response{Error: false, Message: "Success get data", Data: res}

		var result, err2 = json.Marshal(obj)
		if err2 != nil {
			http.Error(w, err2.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(result)
		return

	}
	http.Error(w, `{"Error": true, "Message": "Only GET method allowed"}`, http.StatusBadRequest)
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	if r.Method == "POST" {

		db, err := ConnectDB()

		if err != nil {
			fmt.Println(err.Error())
			return
		}

		defer db.Close()

		var id = r.FormValue("id")
		var name = r.FormValue("name")
		var age = r.FormValue("age")
		var grade = r.FormValue("grade")

		// query cek usernya ada atau tidak
		rows, err := db.Query("SELECT id FROM tb_student WHERE id = ?", id)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()

		var res []student

		for rows.Next() {
			var s = student{}
			var err = rows.Scan(&s.Id)

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			res = append(res, s)
		}

		if res != nil {
			var obj interface{} = &response{Error: true, Message: "Data sudah ada", Data: res}
			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, err2.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(result)
			return
		}
		// end cek user

		_, err = db.Exec("INSERT INTO tb_student VALUES (?,?,?,?)", id, name, age, grade)
		if err != nil {
			// fmt.Println(err.Error())
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		}

		var obj interface{} = &response{Error: false, Message: "Success insert data"}

		var result, err2 = json.Marshal(obj)
		if err2 != nil {
			http.Error(w, err2.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(result)
		return

	}
	http.Error(w, `{"Error": true, "Message": "Only POST method allowed"}`, http.StatusBadRequest)

}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	if r.Method == "POST" {
		var id = r.URL.Query()["id"]

		var name = r.FormValue("name")
		var age = r.FormValue("age")
		var grade = r.FormValue("grade")

		db, err := ConnectDB()
		if err != nil {
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		}
		defer db.Close()

		// query cek usernya ada atau tidak
		rows, err := db.Query("SELECT id,name,age,grade FROM tb_student WHERE id = ?", id[0])
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()

		var res []student

		for rows.Next() {
			var s = student{}
			var err = rows.Scan(&s.Id, &s.Name, &s.Age, &s.Grade)

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			res = append(res, s)
		}

		if res == nil {
			var obj interface{} = &response{Error: true, Message: "Data tidak ada", Data: res}
			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, err2.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(result)
			return
		}
		// end cek user

		_, err = db.Exec("UPDATE tb_student SET name=?,age=?,grade=? WHERE id=?", name, age, grade, id[0])
		if err != nil {
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		} else {
			var obj interface{} = &response{Error: false, Message: "Success upate data"}

			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			w.Write(result)
			return
		}
	}
	http.Error(w, `{"Error": true, "Message": "Only POST method allowed"}`, http.StatusBadRequest)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	if r.Method == "POST" {
		var id = r.URL.Query()["id"]

		db, err := ConnectDB()
		if err != nil {
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		}
		defer db.Close()

		// query cek usernya ada atau tidak
		rows, err := db.Query("SELECT id,name,age,grade FROM tb_student WHERE id = ?", id[0])
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()

		var res []student

		for rows.Next() {
			var s = student{}
			var err = rows.Scan(&s.Id, &s.Name, &s.Age, &s.Grade)

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			res = append(res, s)
		}

		if res == nil {
			var obj interface{} = &response{Error: true, Message: "Data tidak ada", Data: res}
			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, err2.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(result)
			return
		}
		// end cek user

		_, err = db.Exec("DELETE FROM tb_student WHERE id = ?", id[0])
		if err != nil {
			http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			return
		} else {
			var obj interface{} = &response{Error: false, Message: "Success delete data"}

			var result, err2 = json.Marshal(obj)
			if err2 != nil {
				http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			w.Write(result)
			return
		}
	}
	http.Error(w, `{"Error": true, "Message": "Only POST method allowed"}`, http.StatusBadRequest)
}
